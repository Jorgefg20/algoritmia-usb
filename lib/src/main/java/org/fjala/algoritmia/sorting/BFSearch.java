package org.fjala.algoritmia.sorting;

import java.util.LinkedList;
import java.util.Queue;

public class BFSearch {

    public static void main(String args[]) {
        BFSearch bfsearch = new BFSearch(6);

        bfsearch.addEdge(0, 1);
        bfsearch.addEdge(0, 3);
        bfsearch.addEdge(0, 4);
        bfsearch.addEdge(4, 5);
        bfsearch.addEdge(2, 1);
        bfsearch.addEdge(4, 1);
        bfsearch.addEdge(3, 1);
        bfsearch.addEdge(5, 4);
        bfsearch.addEdge(5, 3);

        bfsearch.BFS(5);
    }

    private int V;
    private LinkedList<Integer> adj[];
    private Queue<Integer> queue;

   public BFSearch(int v) {

        this.V = v;
        this.adj = new LinkedList[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new LinkedList<>();
        }
        this.queue = new LinkedList<Integer>();
    }

    public void addEdge(int v, int w) {
        adj[v].add(w);
    }

    public void BFS(int n) {

        boolean nodes[] = new boolean[V];
        int a = 0;
        nodes[n] = true;
        queue.add(n);
        while (queue.size() != 0) {
            n = queue.poll();
            System.out.print(n + " ");

            for (int i = 0; i < adj[n].size(); i++) {
                a = adj[n].get(i);
                if (!nodes[a]) {
                    nodes[a] = true;
                    queue.add(a);
                }
            }
        }
    }

}