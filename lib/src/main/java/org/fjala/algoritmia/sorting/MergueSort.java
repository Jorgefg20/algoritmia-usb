package org.fjala.algoritmia.sorting;

public class MergueSort {

    public MergueSort() {
    }

    public void combinacion(int array[], int izquierda, int medio, int derecha) {
        int n1 = medio - izquierda + 1;
        int n2 = derecha - medio;

        int arrayIzquierdo[] = new int[n1];
        int arrayDerecho[] = new int[n2];

        for (int i = 0; i < n1; i++) {
            arrayIzquierdo[i] = array[izquierda + i];
        }
        for (int j = 0; j < n2; j++) {
            arrayDerecho[j] = array[medio + j + 1];
        }
      
        int i = 0;
        int j = 0;
        int k = izquierda;
 
        while (i < n1 && j < n2) {
            if (arrayIzquierdo[i] <= arrayDerecho[j]) {
                array[k] = arrayIzquierdo[i];
                i++;
            } else {
                array[k] = arrayDerecho[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            array[k] = arrayIzquierdo[i];
            i++;
            k++;
        }
        
        while (j < n2) {
            array[k] = arrayDerecho[j];
            j++;
            k++;
        }
    }

    public void mergeSort(int array[], int izquierda, int derecha){
        
        if(izquierda < derecha){
          int medio = (izquierda + derecha) / 2;
          mergeSort(array, izquierda, medio);
          mergeSort(array, medio+1, derecha);
          combinacion(array, izquierda, medio, derecha);
        }
    }

    public int[] mergesort(int[] array) {
        mergeSort(array, 0, array.length - 1);
        return array;
    }

}
