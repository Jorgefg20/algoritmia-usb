package org.fjala.algoritmia.sorting;

import java.io.IOException;

public class LuckyII {
    public static void main(String[] args) throws IOException {
        LuckyII luckyII = new LuckyII("bmua");
    }

    public LuckyII(String palabra) {
        resultSolve(palabra);
    }

    public String lexicographic(String palabra, int i) {
        int count = 0;
        String left = palabra.substring(0, i);
        char letter = palabra.charAt(i);
        char[] caracteres = left.toCharArray();
        caracteres = quicksort(caracteres);
        for (int j = 0; j < caracteres.length; j++) {
            if (caracteres[j] < letter) {
                count++;
            }
        }
        return "" + count;
    }

    public void resultSolve(String palabra) {
        for (int pivote = 0; pivote < palabra.length(); pivote++) {
            String result = lexicographic(palabra, pivote);
            System.out.println(result);
        }
    }

    public char[] quicksort(char[] array) {
        quicksort(array, 0, array.length - 1);
        return array;
    }

    public void quicksort(char[] array, int izquierdo, int derecho) {
        if (izquierdo < derecho) {
            int pivotPos = partition(array, izquierdo, derecho);
            quicksort(array, izquierdo, pivotPos - 1);
            quicksort(array, pivotPos + 1, derecho);
        }
    }

    public int partition(char[] array, int left, int right) {
        int pivot = array[right];
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (array[j] <= pivot) {
                i++;
                swap(array, i, j);
            }
        }
        swap(array, i + 1, right);
        return i + 1;
    }

    public void swap(char[] array, int first, int second) {
        var temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }

}
