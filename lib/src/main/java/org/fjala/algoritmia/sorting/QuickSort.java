package org.fjala.algoritmia.sorting;

public class QuickSort {

    public QuickSort(){}
    
    public int particion(int array[], int izquierda, int derecha) {

        int pivote = array[izquierda];
        while (true) {
            while (array[izquierda] < pivote) {
                izquierda++;
            }
            while (array[derecha] > pivote) {
                derecha--;
            }

            if (izquierda >= derecha) {
                return derecha;
            } else {
                int temporal = array[izquierda];
                array[izquierda] = array[derecha];
                array[derecha] = temporal;
                izquierda++;
                derecha--;
            }
        }
    }

    public void quickSort(int array[], int izquierda, int derecha) {
        if (izquierda < derecha) {
            int indiceParticion = particion(array, izquierda, derecha);
            quickSort(array, izquierda, indiceParticion);
            quickSort(array, indiceParticion + 1, derecha);
        }
    }

    public int[] quicksort(int[] array) {
        quickSort(array, 0, array.length - 1);
        return array;
    }


}
