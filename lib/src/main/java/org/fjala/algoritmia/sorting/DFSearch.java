package org.fjala.algoritmia.sorting;

import java.util.*;

public class DFSearch {

    public static void main(String args[]) {
        DFSearch dfsearch = new DFSearch(6);

        dfsearch.addEdge(0, 1);
        dfsearch.addEdge(0, 2);
        dfsearch.addEdge(2, 3);
        dfsearch.addEdge(3, 4);
        dfsearch.addEdge(3, 5);
        dfsearch.addEdge(4, 3);
        dfsearch.addEdge(5, 3);

        dfsearch.DFS(2);
    }


    private LinkedList<Integer> adjLists[];
    private boolean visited[];

    public DFSearch(int vertices) {

        adjLists = new LinkedList[vertices];
        visited = new boolean[vertices];

        for (int i = 0; i < vertices; i++)
            adjLists[i] = new LinkedList<Integer>();
    }

    public void addEdge(int src, int dest) {
        adjLists[src].add(dest);
    }

    public void DFS(int vertex) {
        visited[vertex] = true;
        System.out.print(vertex + " ");

        Iterator<Integer> ite = adjLists[vertex].listIterator();
        while (ite.hasNext()) {
            int adj = ite.next();
            if (!visited[adj])
                DFS(adj);
        }
    }

    

}