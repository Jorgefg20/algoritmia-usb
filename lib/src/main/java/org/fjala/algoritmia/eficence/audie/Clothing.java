package org.fjala.algoritmia.eficence.audie;

public class Clothing {
    

  private String brand;
  private String type;
  private String color;


  public Clothing(String brand, String typr, String color){

    this.brand=brand;
    this.type=typr;
    this.color=color;
  }

  public String getBrand() {
      return brand;
  }

  public String getType() {
      return type;
  }

  public String getColor() {
      return color;
  }

  
}
