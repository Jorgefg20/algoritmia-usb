package org.fjala.algoritmia.eficence;
import java.util.ArrayList;
import java.util.List;

public class JulianFest {
    
   public static void main(String[] args) {
       String[] p =new String[] {"A100","A10","A200","A150","L200","A150"};

       JulianFest julianFest = new JulianFest();
       julianFest.CheckGuest(6, p);
       System.out.println("the order is O(n)^2 ");


   }

    public void CheckGuest(int capacity, String[]guests) {
        int peopleInHouse=0;
        List<Integer> valuesGift = new ArrayList<>();

        for (String string : guests) {
            char code = string.charAt(0);
            Integer value = Integer.parseInt(string.substring(1));

            if (code =='A' ) {
                if (valuesGift.isEmpty()) {
                    valuesGift.add(value);
                    peopleInHouse++;
                    System.out.println(string +"|"+ "YES" );
                }else {
                    if (valuesGift.get(valuesGift.size()-1) < value && peopleInHouse< capacity ) {
                       valuesGift.add(value);
                       peopleInHouse++;
                       System.out.println(string +"|"+ "YES" );
                    }else{
                        System.out.println(string +"|"+ "No" );
                    }
                

                }
                
            }else {
                valuesGift.remove(value);
                peopleInHouse--;
                System.out.println(string + "--");

            }
        }
    }

}
