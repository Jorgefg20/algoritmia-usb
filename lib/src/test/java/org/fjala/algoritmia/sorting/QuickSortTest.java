package org.fjala.algoritmia.sorting;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class QuickSortTest {
    
    @Test 
    public void testMergueSort() {
        
        int array [] = {5,26,12,6,1,4,7};

       QuickSort quickSort = new QuickSort();

        int expected[] = {1, 4, 5, 6, 7, 12, 26 };
        int actual[] =quickSort.quicksort(array);
        
        assertArrayEquals(expected, actual);
    }

}
